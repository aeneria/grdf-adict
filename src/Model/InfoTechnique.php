<?php

declare(strict_types=1);

namespace Aeneria\GrdfAdictApi\Model;

/**
 * {
 *  "pce": {
 *   "id_pce": "GI123456"
 *  },
 *  "donnees_techniques": {
 *   "situation_compteur": {
 *    "numero_rue": "9",
 *    "nom_rue": "ALLEE PIERRE AUGUSTE RENOIR",
 *    "complement_adresse": "ut id esse",
 *    "code_postal": "59100",
 *    "commune": "ROUBAIX"
 *   },
 *   "caracteristiques_compteur": {
 *    "frequence": "6M",
 *    "client_sensible_mig": "Oui"
 *   },
 *   "pitd": {
 *    "identifiant_pitd": "GD0991",
 *    "libelle_pitd": "LILLE"
 *   }
 *  },
 *  "statut_restitution": {}
 * }
 *
 */
class InfoTechnique
{
    public string|null $pce;
    public string|null $numeroRue;
    public string|null $nomRue;
    public string|null $complementAdresse;
    public string|null $codePostal;
    public string|null $commune;
    public string $rawData;

    public static function fromJson(string $jsonData): self
    {
        $info = new self();
        $info->rawData = $jsonData;

        $data = \json_decode($jsonData);

        $info->pce = $data->pce->id_pce;
        $data = $data->donnees_techniques->situation_compteur;

        $info->numeroRue = $data->numero_rue ?? null;
        $info->nomRue = $data->nom_rue ?? null;
        $info->complementAdresse = $data->complement_adresse ?? null;
        $info->codePostal = $data->code_postal ?? null;
        $info->commune = $data->commune ?? null;

        return $info;
    }

    public function __toString()
    {
        $parts = [];

        if ($this->numeroRue) {
            $parts[] = $this->numeroRue;
        }

        if ($this->nomRue) {
            $parts[] = $this->nomRue;
        }

        if ($this->complementAdresse) {
            $parts[] = $this->complementAdresse;
        }

        if ($this->codePostal) {
            $parts[] = $this->codePostal;
        }

        if ($this->commune) {
            $parts[] = $this->commune;
        }

        return \implode(", ", $parts);
    }
}
