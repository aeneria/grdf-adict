<?php

declare(strict_types=1);

namespace Aeneria\GrdfAdictApi\Exception;

class GrdfAdictQuotaExceededException extends GrdfAdictException {}
